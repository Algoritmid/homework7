
import java.util.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

/**
 * n�itena on v�etud peamiselt http://rosettacode.org/wiki/Huffman_coding ja varasema kursuse �li�pilase t��d. T�ies osas ei olnud v�imeline ise seda koodi lahnedust looma.
 * Prefix codes and Huffman tree.
 * Tree depends on source data.
 */
public class Huffman {


    private int[] tahe_sagedus;
    private Map<Byte, Leaf> tabel_tahed_kaared;
    private int kodeeringu_pikkus;
   // TODO!!! Your instance variables here!

   /** Constructor to build the Huffman code for a given bytearray.
    * @param original source data
    */
   Huffman (byte[] original) {
       tahe_sagedus = new int[256];
       tabel_tahed_kaared = new HashMap<Byte, Leaf>();
       kodeeringu_pikkus = 0;
       init(original);
      // TODO!!! Your constructor here!
   }

   /** Length of encoded data in bits.
    * @return number of bits
    */
   public int bitLength() {
      return kodeeringu_pikkus; // TODO!!!
   }


   /** Encoding the byte array using this prefixcode.
    * @param origData original data
    * @return encoded data
    */
   public byte[] encode (byte [] origData) {
       StringBuffer tmp = new StringBuffer();
       for (byte b : origData)
           tmp.append(tabel_tahed_kaared.get(b).code);
       kodeeringu_pikkus = tmp.length();
       List<Byte> bytes = new ArrayList<Byte>();
       while (tmp.length() > 0) {
           while (tmp.length() < 8)//teeb baitideks 1 bait 8 baiti
               tmp.append('0');//
           String str = tmp.substring(0, 8);
           bytes.add((byte) Integer.parseInt(str, 2));
           tmp.delete(0, 8);
       }
       byte[] ret = new byte[bytes.size()];
       for (int i = 0; i < bytes.size(); i++)
           ret[i] = bytes.get(i);
       return ret;
       // TODO!!!
   }

   /** Decoding the byte array using this prefixcode.
    * @param encodedData encoded data
    * @return decoded data (hopefully identical to original)
    */
   public byte[] decode (byte[] encodedData) {
       StringBuffer tmp = new StringBuffer();
       for (int i = 0; i < encodedData.length; i++) //pikkuseks on kodeeritud ja baitideks (jagada 8-ga) listi pikkus (antud hetkel 4 = 29/8)
           tmp.append(String.format("%8s", Integer.toBinaryString(encodedData[i] & 0xFF)).replace(' ', '0')); //v'ga keeruline koht
       String str = tmp.substring(0, kodeeringu_pikkus);
       List<Byte> bytes = new ArrayList<Byte>();
       String code = "";
       while (str.length() > 0) {
           code += str.substring(0, 1);
           str = str.substring(1);
           Iterator<Leaf> list = tabel_tahed_kaared.values().iterator();
           while (list.hasNext()) {
               Leaf leaf = list.next();
               if (leaf.code.equals(code)) {
                   bytes.add(leaf.value);
                   code = "";
                   break;
               }
           }
       }
       byte[] ret = new byte[bytes.size()];
       for (int i = 0; i < bytes.size(); i++)
           ret[i] = bytes.get(i);
       return ret;
       // TODO!!!
   }

   /** Main method. */
   public static void main (String[] params) {
      String tekst = "AAAAAAAAAAAAABBBBBBCCCDDEEF";
      byte[] orig = tekst.getBytes();
      Huffman huf = new Huffman (orig);
      byte[] kood = huf.encode (orig);
      byte[] orig2 = huf.decode (kood);
      // must be equal: orig, orig2
      System.out.println (Arrays.equals (orig, orig2));
      int lngth = huf.bitLength();
      System.out.println ("Length of encoded data in bits: " + lngth);
      // TODO!!! Your tests here!
   }

    private void init(byte[] data) {
        for (byte b : data)
            tahe_sagedus[b]++; //siin lisab b vaartusele tema sageduse nt A kohal 65 summa on 4 jne
        PriorityQueue<Tree> trees = new PriorityQueue<Tree>();
        for (int i = 0; i < tahe_sagedus.length; i++) // siin k'ib kogu listi l'bi 265
            if (tahe_sagedus[i] > 0)
                trees.offer(new Leaf(tahe_sagedus[i], (byte) i));
        assert trees.size() > 0;
        while (trees.size() > 1)
            trees.offer(new Node(trees.poll(), trees.poll()));
        Tree tree = trees.poll();
        code(tree, new StringBuffer());
    }

    private void code(Tree tree, StringBuffer prefix) {
        assert tree != null;
        if (tree instanceof Leaf) {
            Leaf leaf = (Leaf) tree;
            leaf.code = (prefix.length() > 0) ? prefix.toString() : "0";
            tabel_tahed_kaared.put(leaf.value, leaf);
        } else if (tree instanceof Node) {
            Node node = (Node) tree;
            prefix.append('0'); //lisab vasakusse jalga 0
            code(node.left, prefix);
            prefix.deleteCharAt(prefix.length() - 1);
            prefix.append('1'); // lisab paremasse jalga 1
            code(node.right, prefix);
            prefix.deleteCharAt(prefix.length() - 1);
        }
    }

    abstract class Tree implements Comparable<Tree> {


        public final int puu_kaal;

        public Tree(int puu_kaal) {
            this.puu_kaal = puu_kaal;
        }

        @Override
        public int compareTo(Tree o) {
            return puu_kaal - o.puu_kaal;
        }

    }


    class Leaf extends Tree { //Huffman puu t�hed


        public final byte value; //puu t�hed paitidena

        public String code;

        public Leaf(int frequency, byte value) {
            super(frequency);
            this.value = value;
        }

    }


    class Node extends Tree { //Huffmani kaared

        public final Tree left, right; //

        public Node(Tree left, Tree right) {
            super(left.puu_kaal + right.puu_kaal);
            this.left = left;
            this.right = right;
        }

    }
}

